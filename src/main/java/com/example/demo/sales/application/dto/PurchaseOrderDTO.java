package com.example.demo.sales.application.dto;

import com.example.demo.common.application.dto.BusinessPeriodDTO;
import com.example.demo.inventory.application.dto.Plant;
import com.example.demo.sales.domain.POStatus;
import lombok.Data;
import org.springframework.hateoas.RepresentationModel;


@Data
public class PurchaseOrderDTO extends RepresentationModel<PurchaseOrderDTO> {
    Long _id;
    BusinessPeriodDTO rentalPeriod;
    Plant plant;
    POStatus status;
}
